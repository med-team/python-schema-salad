Source: python-schema-salad
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Michael R. Crusoe <crusoe@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               dh-sequence-sphinxdoc <!nodoc>,
               python3,
               python3-all,
               python3-all-dev:native,
               libpython3-all-dev,
               python3-setuptools,
               python3-setuptools-scm,
               python3-mistune (>> 3),
               python3-requests,
               python3-yaml,
               python3-rdflib,
               python3-ruamel.yaml,
               python3-mypy,
               python3-typeshed,
               python3-cachecontrol ( >> 0.12.12 ),
               python3-filelock,
               pybuild-plugin-pyproject,
               python3-pytest-xdist <!nocheck>,
               python3-pytest (>> 6.2) <!nocheck>,
               python3-pytest-runner <!nocheck>,
               black,
               python3-typing-extensions <!nocheck>,
               help2man,
               python3-sphinx-autoapi <!nodoc>,
               python3-sphinx-autodoc-typehints <!nodoc>,
               python3-sphinx-rtd-theme <!nodoc>,
               python3-sphinxcontrib.autoprogram <!nodoc>,
               python3-doc <!nodoc>,
               python-rdflib-doc <!nodoc>,
               python-requests-doc <!nodoc>
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-schema-salad
Vcs-Git: https://salsa.debian.org/python-team/packages/python-schema-salad.git
Homepage: https://schema-salad.readthedocs.io/
Rules-Requires-Root: no

Package: python3-schema-salad
Architecture: any
Depends: ${python3:Depends},
         python3-mistune ( >> 3),
         python3-filelock,
         ${misc:Depends},
         ${shlibs:Depends},
         libjs-bootstrap
Recommends: cwltool, black
Breaks: cwltool (<< 3.1.20210521105815)
Description: Schema Annotations for Linked Avro Data (SALAD)
 Salad is a schema language for describing JSON or YAML structured linked data
 documents. Salad is based originally on JSON-LD and the Apache Avro data
 serialization system.
 .
 Salad schema describes rules for preprocessing, structural validation, and
 link checking for documents described by a Salad schema. Salad features for
 rich data modeling such as inheritance, template specialization, object
 identifiers, object references, documentation generation, and transformation
 to RDF. Salad provides a bridge between document and record oriented data
 modeling and the Semantic Web.
