.coveragerc
.dockerignore
.flake8
.git-blame-ignore-revs
.gitignore
.isort.cfg
.mergify.yml
.pylintrc
.readthedocs.yml
CONTRIBUTING.md
LICENSE.txt
MANIFEST.in
Makefile
README.rst
build-schema_salad-docker.sh
cibw-requirements.txt
dev-requirements.txt
lgtm.yml
lint-requirements.txt
manual_wheel_publish.sh
mypy-requirements.txt
mypy.ini
pyproject.toml
release-test.sh
requirements.txt
schema_salad.Dockerfile
setup.cfg
setup.py
test-requirements.txt
tox.ini
.circleci/config.yml
.github/dependabot.yml
.github/workflows/ci-tests.yml
.github/workflows/codeql-analysis.yml
.github/workflows/quay-publish.yml
.github/workflows/wheels.yml
docs/Makefile
docs/cli.rst
docs/conf.py
docs/index.rst
docs/make.bat
docs/typeshed.rst
docs/_static/favicon.ico
mypy-stubs/mistune/__init__.pyi
mypy-stubs/mistune/block_parser.pyi
mypy-stubs/mistune/core.pyi
mypy-stubs/mistune/helpers.pyi
mypy-stubs/mistune/inline_parser.pyi
mypy-stubs/mistune/list_parser.pyi
mypy-stubs/mistune/markdown.pyi
mypy-stubs/mistune/toc.pyi
mypy-stubs/mistune/util.pyi
mypy-stubs/mistune/directives/__init__.pyi
mypy-stubs/mistune/directives/_base.pyi
mypy-stubs/mistune/directives/_fenced.pyi
mypy-stubs/mistune/directives/_rst.pyi
mypy-stubs/mistune/directives/admonition.pyi
mypy-stubs/mistune/directives/image.pyi
mypy-stubs/mistune/directives/include.pyi
mypy-stubs/mistune/directives/toc.pyi
mypy-stubs/mistune/plugins/__init__.pyi
mypy-stubs/mistune/plugins/abbr.pyi
mypy-stubs/mistune/plugins/def_list.pyi
mypy-stubs/mistune/plugins/footnotes.pyi
mypy-stubs/mistune/plugins/formatting.pyi
mypy-stubs/mistune/plugins/math.pyi
mypy-stubs/mistune/plugins/ruby.pyi
mypy-stubs/mistune/plugins/speedup.pyi
mypy-stubs/mistune/plugins/spoiler.pyi
mypy-stubs/mistune/plugins/table.pyi
mypy-stubs/mistune/plugins/task_lists.pyi
mypy-stubs/mistune/plugins/url.pyi
mypy-stubs/mistune/renderers/__init__.pyi
mypy-stubs/mistune/renderers/_list.pyi
mypy-stubs/mistune/renderers/html.pyi
mypy-stubs/mistune/renderers/markdown.pyi
mypy-stubs/mistune/renderers/rst.pyi
mypy-stubs/rdflib/__init__.pyi
mypy-stubs/rdflib/collection.pyi
mypy-stubs/rdflib/compare.pyi
mypy-stubs/rdflib/exceptions.pyi
mypy-stubs/rdflib/graph.pyi
mypy-stubs/rdflib/parser.pyi
mypy-stubs/rdflib/paths.pyi
mypy-stubs/rdflib/plugin.pyi
mypy-stubs/rdflib/query.pyi
mypy-stubs/rdflib/resource.pyi
mypy-stubs/rdflib/term.pyi
mypy-stubs/rdflib/util.pyi
mypy-stubs/rdflib/namespace/_CSVW.pyi
mypy-stubs/rdflib/namespace/_DC.pyi
mypy-stubs/rdflib/namespace/_DCAT.pyi
mypy-stubs/rdflib/namespace/_DCTERMS.pyi
mypy-stubs/rdflib/namespace/_DOAP.pyi
mypy-stubs/rdflib/namespace/_FOAF.pyi
mypy-stubs/rdflib/namespace/_ODRL2.pyi
mypy-stubs/rdflib/namespace/_ORG.pyi
mypy-stubs/rdflib/namespace/_OWL.pyi
mypy-stubs/rdflib/namespace/_PROF.pyi
mypy-stubs/rdflib/namespace/_PROV.pyi
mypy-stubs/rdflib/namespace/_QB.pyi
mypy-stubs/rdflib/namespace/_RDF.pyi
mypy-stubs/rdflib/namespace/_RDFS.pyi
mypy-stubs/rdflib/namespace/_SDO.pyi
mypy-stubs/rdflib/namespace/_SH.pyi
mypy-stubs/rdflib/namespace/_SKOS.pyi
mypy-stubs/rdflib/namespace/_SOSA.pyi
mypy-stubs/rdflib/namespace/_SSN.pyi
mypy-stubs/rdflib/namespace/_TIME.pyi
mypy-stubs/rdflib/namespace/_VOID.pyi
mypy-stubs/rdflib/namespace/_XSD.pyi
mypy-stubs/rdflib/namespace/__init__.pyi
mypy-stubs/rdflib/plugins/__init__.pyi
mypy-stubs/rdflib/plugins/parsers/__init__.pyi
mypy-stubs/rdflib/plugins/parsers/notation3.pyi
mypy-stubs/ruamel/__init__.py
schema_salad/__init__.py
schema_salad/__main__.py
schema_salad/_version.py
schema_salad/codegen.py
schema_salad/codegen_base.py
schema_salad/cpp_codegen.py
schema_salad/dlang_codegen.py
schema_salad/dotnet_codegen.py
schema_salad/exceptions.py
schema_salad/fetcher.py
schema_salad/java_codegen.py
schema_salad/jsonld_context.py
schema_salad/main.py
schema_salad/makedoc.py
schema_salad/metaschema.py
schema_salad/py.typed
schema_salad/python_codegen.py
schema_salad/python_codegen_support.py
schema_salad/ref_resolver.py
schema_salad/schema.py
schema_salad/sourceline.py
schema_salad/typescript_codegen.py
schema_salad/utils.py
schema_salad/validate.py
schema_salad.egg-info/PKG-INFO
schema_salad.egg-info/SOURCES.txt
schema_salad.egg-info/dependency_links.txt
schema_salad.egg-info/entry_points.txt
schema_salad.egg-info/requires.txt
schema_salad.egg-info/top_level.txt
schema_salad.egg-info/zip-safe
schema_salad/avro/LICENSE
schema_salad/avro/NOTICE
schema_salad/avro/__init__.py
schema_salad/avro/schema.py
schema_salad/dotnet/AssemblyInfo.cs
schema_salad/dotnet/LICENSE
schema_salad/dotnet/Project.csproj.template
schema_salad/dotnet/README.md
schema_salad/dotnet/Solution.sln
schema_salad/dotnet/Test.csproj.template
schema_salad/dotnet/docfx.json
schema_salad/dotnet/editorconfig
schema_salad/dotnet/gitignore
schema_salad/dotnet/DocFx/filterConfig.yml
schema_salad/dotnet/DocFx/index.md
schema_salad/dotnet/DocFx/toc.yml
schema_salad/dotnet/Test/runsettings.xml
schema_salad/dotnet/Test/data/test.txt
schema_salad/dotnet/Test/src/ExampleTests.cs
schema_salad/dotnet/Test/src/FetcherTests.cs
schema_salad/dotnet/Test/src/UtilitiesTests.cs
schema_salad/dotnet/Test/src/loaders/AnyLoaderTests.cs
schema_salad/dotnet/Test/src/loaders/ArrayLoaderTests.cs
schema_salad/dotnet/Test/src/loaders/EnumLoaderTests.cs
schema_salad/dotnet/Test/src/loaders/ExpressionLoaderTests.cs
schema_salad/dotnet/Test/src/loaders/IdMapLoaderTests.cs
schema_salad/dotnet/Test/src/loaders/NullLoaderTests.cs
schema_salad/dotnet/Test/src/loaders/UnionLoaderTests.cs
schema_salad/dotnet/Test/src/loaders/PrimtiveLoader/PrimitiveLoaderDoubleTests.cs
schema_salad/dotnet/Test/src/loaders/PrimtiveLoader/PrimitiveLoaderFloatTests.cs
schema_salad/dotnet/Test/src/loaders/PrimtiveLoader/PrimitiveLoaderIntTests.cs
schema_salad/dotnet/Test/src/loaders/PrimtiveLoader/PrimitiveLoaderStringTests.cs
schema_salad/dotnet/util/Fetcher.cs
schema_salad/dotnet/util/LoaderInstances.cs
schema_salad/dotnet/util/LoadingOptions.cs
schema_salad/dotnet/util/Saveable.cs
schema_salad/dotnet/util/ScalarNodeTypeResolver.cs
schema_salad/dotnet/util/UriExtensions.cs
schema_salad/dotnet/util/Utilities.cs
schema_salad/dotnet/util/ValidationException.cs
schema_salad/dotnet/util/Vocabs.cs
schema_salad/dotnet/util/Loaders/AnyLoader.cs
schema_salad/dotnet/util/Loaders/ArrayLoader.cs
schema_salad/dotnet/util/Loaders/EnumLoader.cs
schema_salad/dotnet/util/Loaders/ExpressionLoader.cs
schema_salad/dotnet/util/Loaders/IdMapLoader.cs
schema_salad/dotnet/util/Loaders/Loader.cs
schema_salad/dotnet/util/Loaders/MapLoader.cs
schema_salad/dotnet/util/Loaders/NullLoader.cs
schema_salad/dotnet/util/Loaders/PrimitiveLoader.cs
schema_salad/dotnet/util/Loaders/RecordLoader.cs
schema_salad/dotnet/util/Loaders/RootLoader.cs
schema_salad/dotnet/util/Loaders/SecondaryDSLLoader.cs
schema_salad/dotnet/util/Loaders/TypeDSLLoader.cs
schema_salad/dotnet/util/Loaders/UnionLoader.cs
schema_salad/dotnet/util/Loaders/UriLoader.cs
schema_salad/java/MANIFEST.MF
schema_salad/java/README.md
schema_salad/java/gitignore
schema_salad/java/overview.html
schema_salad/java/package.html
schema_salad/java/pom.xml
schema_salad/java/main_utils/AnyLoader.java
schema_salad/java/main_utils/ArrayLoader.java
schema_salad/java/main_utils/ConstantMaps.java
schema_salad/java/main_utils/DefaultFetcher.java
schema_salad/java/main_utils/EnumLoader.java
schema_salad/java/main_utils/ExpressionLoader.java
schema_salad/java/main_utils/Fetcher.java
schema_salad/java/main_utils/IdMapLoader.java
schema_salad/java/main_utils/Loader.java
schema_salad/java/main_utils/LoaderInstances.java
schema_salad/java/main_utils/LoadingOptions.java
schema_salad/java/main_utils/LoadingOptionsBuilder.java
schema_salad/java/main_utils/MapLoader.java
schema_salad/java/main_utils/NullLoader.java
schema_salad/java/main_utils/OneOrListOf.java
schema_salad/java/main_utils/OneOrListOfLoader.java
schema_salad/java/main_utils/OptionalLoader.java
schema_salad/java/main_utils/PrimitiveLoader.java
schema_salad/java/main_utils/RecordLoader.java
schema_salad/java/main_utils/RootLoader.java
schema_salad/java/main_utils/Saveable.java
schema_salad/java/main_utils/SaveableImpl.java
schema_salad/java/main_utils/SecondaryFilesDslLoader.java
schema_salad/java/main_utils/TypeDslLoader.java
schema_salad/java/main_utils/UnionLoader.java
schema_salad/java/main_utils/UriLoader.java
schema_salad/java/main_utils/Uris.java
schema_salad/java/main_utils/ValidationException.java
schema_salad/java/main_utils/Validator.java
schema_salad/java/main_utils/YamlUtils.java
schema_salad/java/main_utils/package.html
schema_salad/java/test_utils/DefaultFetcherTest.java
schema_salad/java/test_utils/ExamplesTest.java
schema_salad/java/test_utils/ShortnameTest.java
schema_salad/java/test_utils/YamlUtilsTest.java
schema_salad/metaschema/field_name.yml
schema_salad/metaschema/field_name_proc.yml
schema_salad/metaschema/field_name_schema.yml
schema_salad/metaschema/field_name_src.yml
schema_salad/metaschema/ident_res.yml
schema_salad/metaschema/ident_res_proc.yml
schema_salad/metaschema/ident_res_schema.yml
schema_salad/metaschema/ident_res_src.yml
schema_salad/metaschema/import_include.md
schema_salad/metaschema/link_res.yml
schema_salad/metaschema/link_res_proc.yml
schema_salad/metaschema/link_res_schema.yml
schema_salad/metaschema/link_res_src.yml
schema_salad/metaschema/map_res.yml
schema_salad/metaschema/map_res_proc.yml
schema_salad/metaschema/map_res_schema.yml
schema_salad/metaschema/map_res_src.yml
schema_salad/metaschema/metaschema.yml
schema_salad/metaschema/metaschema_base.yml
schema_salad/metaschema/salad.md
schema_salad/metaschema/sfdsl_res.yml
schema_salad/metaschema/sfdsl_res_proc.yml
schema_salad/metaschema/sfdsl_res_schema.yml
schema_salad/metaschema/sfdsl_res_src.yml
schema_salad/metaschema/typedsl_res.yml
schema_salad/metaschema/typedsl_res_proc.yml
schema_salad/metaschema/typedsl_res_schema.yml
schema_salad/metaschema/typedsl_res_src.yml
schema_salad/metaschema/vocab_res.yml
schema_salad/metaschema/vocab_res_proc.yml
schema_salad/metaschema/vocab_res_schema.yml
schema_salad/metaschema/vocab_res_src.yml
schema_salad/tests/EDAM.owl
schema_salad/tests/Process.yml
schema_salad/tests/__init__.py
schema_salad/tests/bad_schema.yml
schema_salad/tests/bad_schema2.yml
schema_salad/tests/basket.yml
schema_salad/tests/basket_schema.yml
schema_salad/tests/class_field_test.yml
schema_salad/tests/conftest.py
schema_salad/tests/cwl-pre.yml
schema_salad/tests/formattest2.cwl
schema_salad/tests/frag.yml
schema_salad/tests/hello.txt
schema_salad/tests/hellofield.yml
schema_salad/tests/inherited-attributes.yml
schema_salad/tests/inject-id1.yml
schema_salad/tests/inject-id2.yml
schema_salad/tests/inject-id3.yml
schema_salad/tests/list.json
schema_salad/tests/matcher.py
schema_salad/tests/memory-leak-check.py
schema_salad/tests/metaschema-pre.yml
schema_salad/tests/missing_step_name.cwl
schema_salad/tests/mixin.yml
schema_salad/tests/multidoc.yml
schema_salad/tests/pt.yml
schema_salad/tests/revtool_bad_schema.cwl
schema_salad/tests/test_avro_names.py
schema_salad/tests/test_cg.py
schema_salad/tests/test_cli_args.py
schema_salad/tests/test_codegen_errors.py
schema_salad/tests/test_cpp_codegen.py
schema_salad/tests/test_cwl11.py
schema_salad/tests/test_dlang_codegen.py
schema_salad/tests/test_dotnet_codegen.py
schema_salad/tests/test_errors.py
schema_salad/tests/test_examples.py
schema_salad/tests/test_fetch.py
schema_salad/tests/test_fp.py
schema_salad/tests/test_java_codegen.py
schema_salad/tests/test_makedoc.py
schema_salad/tests/test_misc.py
schema_salad/tests/test_pickling.py
schema_salad/tests/test_print_oneline.py
schema_salad/tests/test_python_codegen.py
schema_salad/tests/test_real_cwl.py
schema_salad/tests/test_ref_resolver.py
schema_salad/tests/test_schema.py
schema_salad/tests/test_schemas_directive.py
schema_salad/tests/test_subtypes.py
schema_salad/tests/test_typescript_codegen.py
schema_salad/tests/util.py
schema_salad/tests/cpp_tests/01_single_record.h
schema_salad/tests/cpp_tests/01_single_record.yml
schema_salad/tests/cpp_tests/02_two_records.h
schema_salad/tests/cpp_tests/02_two_records.yml
schema_salad/tests/cpp_tests/03_simple_inheritance.h
schema_salad/tests/cpp_tests/03_simple_inheritance.yml
schema_salad/tests/cpp_tests/04_abstract_inheritance.h
schema_salad/tests/cpp_tests/04_abstract_inheritance.yml
schema_salad/tests/cpp_tests/05_specialization.h
schema_salad/tests/cpp_tests/05_specialization.yml
schema_salad/tests/docimp/d1.yml
schema_salad/tests/docimp/d2.md
schema_salad/tests/docimp/d3.yml
schema_salad/tests/docimp/d4.yml
schema_salad/tests/docimp/d5.md
schema_salad/tests/docimp/dpre.json
schema_salad/tests/foreign/foreign_prop1.cwl
schema_salad/tests/foreign/foreign_prop2.cwl
schema_salad/tests/foreign/foreign_prop3.cwl
schema_salad/tests/foreign/foreign_prop4.cwl
schema_salad/tests/foreign/foreign_prop5.cwl
schema_salad/tests/foreign/foreign_prop6.cwl
schema_salad/tests/foreign/foreign_prop7.cwl
schema_salad/tests/test_real_cwl/tabs_rna_seq_workflow.cwl
schema_salad/tests/test_real_cwl/ICGC-TCGA-PanCancer/PreprocessedFilesType.yaml
schema_salad/tests/test_real_cwl/ICGC-TCGA-PanCancer/preprocess_util.js
schema_salad/tests/test_real_cwl/ICGC-TCGA-PanCancer/preprocess_vcf.cwl
schema_salad/tests/test_real_cwl/ICGC-TCGA-PanCancer/vcf_merge_util.js
schema_salad/tests/test_real_cwl/ICGC-TCGA-PanCancer/complete/PreprocessedFilesType.yaml
schema_salad/tests/test_real_cwl/ICGC-TCGA-PanCancer/complete/clean_vcf.cwl
schema_salad/tests/test_real_cwl/ICGC-TCGA-PanCancer/complete/extract_snv.cwl
schema_salad/tests/test_real_cwl/ICGC-TCGA-PanCancer/complete/normalize.cwl
schema_salad/tests/test_real_cwl/ICGC-TCGA-PanCancer/complete/pass-filter.cwl
schema_salad/tests/test_real_cwl/ICGC-TCGA-PanCancer/complete/preprocess_util.js
schema_salad/tests/test_real_cwl/ICGC-TCGA-PanCancer/complete/preprocess_vcf.cwl
schema_salad/tests/test_real_cwl/ICGC-TCGA-PanCancer/complete/vcf_merge.cwl
schema_salad/tests/test_real_cwl/ICGC-TCGA-PanCancer/complete/vcf_merge_util.js
schema_salad/tests/test_real_cwl/bio-cwl-tools/bamtools_stats.cwl
schema_salad/tests/test_real_cwl/bio-cwl-tools/bamtools_stats_invalid_schema_ref.cwl
schema_salad/tests/test_real_cwl/bio-cwl-tools/picard_CreateSequenceDictionary.cwl
schema_salad/tests/test_real_cwl/h3agatk/GATK-Sub-Workflow-h3abionet-snp.cwl
schema_salad/tests/test_real_cwl/h3agatk/GATK-complete-WES-Workflow-h3abionet.cwl
schema_salad/tests/test_real_cwl/topmed/topmed_variant_calling_pipeline.cwl
schema_salad/tests/test_schema/Base.yml
schema_salad/tests/test_schema/CommandLineTool.yml
schema_salad/tests/test_schema/CommonWorkflowLanguage.yml
schema_salad/tests/test_schema/Process.yml
schema_salad/tests/test_schema/Workflow.yml
schema_salad/tests/test_schema/avro_naming.yml
schema_salad/tests/test_schema/avro_naming_base.yml
schema_salad/tests/test_schema/avro_subtype.yml
schema_salad/tests/test_schema/avro_subtype_bad.yml
schema_salad/tests/test_schema/avro_subtype_nested.yml
schema_salad/tests/test_schema/avro_subtype_nested_bad.yml
schema_salad/tests/test_schema/avro_subtype_recursive.yml
schema_salad/tests/test_schema/avro_subtype_union.yml
schema_salad/tests/test_schema/avro_subtype_union_bad.yml
schema_salad/tests/test_schema/concepts.md
schema_salad/tests/test_schema/contrib.md
schema_salad/tests/test_schema/cwltest-schema.yml
schema_salad/tests/test_schema/intro.md
schema_salad/tests/test_schema/invocation.md
schema_salad/tests/test_schema/metaschema_base.yml
schema_salad/tests/test_schema/misc_schema_v1.yml
schema_salad/tests/test_schema/misc_schema_v2.yml
schema_salad/tests/test_schema/no_field_schema.yml
schema_salad/tests/test_schema/one_line_primary_doc.yml
schema_salad/tests/test_schema/test1.cwl
schema_salad/tests/test_schema/test10.cwl
schema_salad/tests/test_schema/test11.cwl
schema_salad/tests/test_schema/test12.cwl
schema_salad/tests/test_schema/test13.cwl
schema_salad/tests/test_schema/test14.cwl
schema_salad/tests/test_schema/test15.cwl
schema_salad/tests/test_schema/test16.cwl
schema_salad/tests/test_schema/test17.cwl
schema_salad/tests/test_schema/test18.cwl
schema_salad/tests/test_schema/test19.cwl
schema_salad/tests/test_schema/test2.cwl
schema_salad/tests/test_schema/test3.cwl
schema_salad/tests/test_schema/test4.cwl
schema_salad/tests/test_schema/test5.cwl
schema_salad/tests/test_schema/test6.cwl
schema_salad/tests/test_schema/test7.cwl
schema_salad/tests/test_schema/test8.cwl
schema_salad/tests/test_schema/test9.cwl
schema_salad/typescript/.gitignore
schema_salad/typescript/LICENSE
schema_salad/typescript/index.ts
schema_salad/typescript/package.json
schema_salad/typescript/tsconfig.json
schema_salad/typescript/test/AnyLoader.spec.ts
schema_salad/typescript/test/ExampleTest.ts
schema_salad/typescript/test/Fetcher.spec.ts
schema_salad/typescript/test/IdMap.spec.ts
schema_salad/typescript/test/LoadingOptions.spec.ts
schema_salad/typescript/test/Typeguards.spec.ts
schema_salad/typescript/test/utilities.spec.ts
schema_salad/typescript/test/data/hellofield.yml
schema_salad/typescript/test/data/test.txt
schema_salad/typescript/util/Dict.ts
schema_salad/typescript/util/Fetcher.ts
schema_salad/typescript/util/Internal.ts
schema_salad/typescript/util/LoaderInstances.ts
schema_salad/typescript/util/LoadingOptions.ts
schema_salad/typescript/util/Saveable.ts
schema_salad/typescript/util/Typeguards.ts
schema_salad/typescript/util/ValidationException.ts
schema_salad/typescript/util/Vocabs.ts
schema_salad/typescript/util/loaders/AnyLoader.ts
schema_salad/typescript/util/loaders/ArrayLoader.ts
schema_salad/typescript/util/loaders/EnumLoader.ts
schema_salad/typescript/util/loaders/ExpressionLoader.ts
schema_salad/typescript/util/loaders/IdMapLoader.ts
schema_salad/typescript/util/loaders/Loader.ts
schema_salad/typescript/util/loaders/MapLoader.ts
schema_salad/typescript/util/loaders/PrimitiveLoader.ts
schema_salad/typescript/util/loaders/RecordLoader.ts
schema_salad/typescript/util/loaders/RootLoader.ts
schema_salad/typescript/util/loaders/SecondaryDSLLoader.ts
schema_salad/typescript/util/loaders/TypeDSLLoader.ts
schema_salad/typescript/util/loaders/UnionLoader.ts
schema_salad/typescript/util/loaders/UriLoader.ts